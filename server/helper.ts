import { NextFunction, Request, Response } from "express";
import hljs from "highlight.js";
import MarkdownIt from "markdown-it";
import { list_repos } from "./lib";

export async function check_repo_exists(req: Request, res: Response, next: NextFunction) {
    const repo_list = await list_repos()
    if (repo_list.includes(req.params.repo)) return next()
    res.status(404).render("error", { message: "this repository doesnt exist" })
}

export function clean_args(a: any): { [key: string]: string } {
    return Object.fromEntries(Object.entries(a).map(([key, value]) => {
        if (typeof value == "string")
            return [key, value]
        else
            throw new Error("aaaa");
    }))
}



const markdown: any = MarkdownIt({
    html: true,
    // highlight: function (str, lang) {
    //     if (lang && hljs.getLanguage(lang)) {
    //         try {
    //             return (
    //                 '<pre><code class="hljs">' +
    //                 hljs.highlight(str, { language: lang, ignoreIllegals: true }).value +
    //                 "</code></pre>"
    //             );
    //         } catch (__) { }
    //     }
    //     return (
    //         '<pre><code class="hljs">' +
    //         markdown.utils.escapeHtml(str) +
    //         "</code></pre>"
    //     );
    // },
})

export function render_markdown(source: string): string {
    return markdown.render(source)
}