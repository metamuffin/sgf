import { basename, dirname } from "path/posix";
import { run_repo_git_command } from "./io";

export async function archive_file_content(repo: string, filepath: string, commit: string): Promise<string> {
    let c;
    if (dirname(filepath) != ".") c = commit + ":" + dirname(filepath)
    else c = commit
    const out = await run_repo_git_command(repo, ["archive", c, basename(filepath)])
    return out
}
