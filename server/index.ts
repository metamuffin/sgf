
import express, { static as estatic } from "express"
import MarkdownIt from "markdown-it"
import { join } from "path/posix"
import { archive_file_content } from "./archive"
import { check_repo_exists, clean_args, render_markdown } from "./helper"
import { get_repo_metadata, list_repos } from "./lib"
import { log_query } from "./log"
import { worktree_ls } from "./worktree"

const app = express()

app.set('view engine', 'pug')
app.set('views', join(__dirname, "../views"))


app.get("/", async (req, res) => res.render("repo-list", {
    repos: await Promise.all((await list_repos()).map(rn => get_repo_metadata(rn)))
}))

app.get("/:repo", check_repo_exists, async (req, res) => res.redirect(`/${req.params.repo}/readme`))

app.get("/:repo/readme", check_repo_exists, async (req, res) => {
    const content = await archive_file_content(req.params.repo, "README.md", "HEAD")
    console.log(content);
    const html = render_markdown(content)

    res.render("repo-readme", {
        repo: await get_repo_metadata(req.params.repo),
        readme: html,
    })
})

app.get("/:repo/commits", check_repo_exists, async (req, res) => {
    const { log, page, next_page, prev_page } = await log_query(req.params.repo, clean_args(req.query))
    res.render("repo-log", {
        log, page, next_page, prev_page,
        repo: await get_repo_metadata(req.params.repo)
    })
})

app.get("/:repo/tree/:commit", (req, res) => res.redirect(req.path + "/"))
app.get("/:repo/tree/:commit/*", check_repo_exists, async (req, res) => {
    //@ts-ignore this makes no sense
    let k = req.params["0"]
    if (k.endsWith("/")) k = k.substring(0, k.length - 1)
    let filepath = k.split("/")
    let final_type;
    if (k.length != 0) {
        for (let i = 0; i < filepath.length; i++) {
            const part = filepath[i];
            const files = await worktree_ls(req.params.repo, req.params.commit, filepath.slice(0, i).join("/"))
            const type = files[part]
            console.log(part, type);
            if (!type) break
            if (i == filepath.length - 1) final_type = type
            if (type == "regular") break
        }
    } else final_type = "directory"
    if (!final_type) {
        res.sendStatus(404)
    } else if (final_type == "regular") {
        if (req.path.endsWith("/")) return res.redirect(req.path.substring(0, req.path.length - 1))
        res.send("todo")
    } else if (final_type == "directory") {
        if (!req.path.endsWith("/")) return res.redirect(req.path + "/")
        const files = await worktree_ls(req.params.repo, req.params.commit, filepath.join("/"))
        res.render("repo-tree", {
            files,
            path: filepath,
            commit: req.params.commit,
            repo: await get_repo_metadata(req.params.repo)
        })
    }

})

app.use("/style", estatic(join(__dirname, "../style")))


app.listen(8080, "127.0.0.1", () => console.log("server listening..."))

