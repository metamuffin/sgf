import { Request } from "express";
import { run_repo_git_command } from "./io";

const DEFAULT_LOG_LIMIT = 50
const MAX_LOG_LIMIT = 100

export interface LogOpts {
    skip?: number
    limit: number
    author?: string
}

export type GitLog = GitCommit[]
export interface GitCommit {
    author: string,
    hash: string
    date: string,
    message: string
}

export async function log_query(repo: string, query: { [key: string]: string }): Promise<{ log: GitLog, page: number, prev_page: string, next_page: string }> {
    let o: LogOpts = { limit: 0 }

    let limit = parseInt(query.limit)
    if (!Number.isNaN(limit)) o.limit = Math.min(limit, MAX_LOG_LIMIT)
    else o.limit = DEFAULT_LOG_LIMIT

    let skip = parseInt(query.skip)
    if (!Number.isNaN(skip)) o.skip = skip

    if (query.author) o.author = query.author
    return {
        log: await log_opts(repo, o),
        page: (o.skip ?? 0) / o.limit + 1,
        next_page: `?skip=${(o.skip ?? 0) + o.limit}`,
        prev_page: `?skip=${Math.max(0, (o.skip ?? 0) - o.limit)}`,
    }
}

export function git_log_args(opts: LogOpts): string[] {
    let a = ["log"]

    a.push("-" + opts.limit)
    if (opts.skip) a.push("--skip", opts.skip.toString())
    if (opts.author) a.push("--author", opts.author)

    return a
}

export function parse_log_output(output: string): GitLog {
    const lines = output.split("\n")
    let a: GitLog = []
    let c: GitCommit | undefined
    for (const line of lines) {
        if (line.startsWith("commit ")) {
            if (c) a.push(c)
            c = { author: "", date: "", hash: "", message: "" }
            c.hash = line.substr("commit ".length)
        }
        else if (line.startsWith("Author:"))
            c!.author = line.substr("Author: ".length).trim()
        else if (line.startsWith("Date:"))
            c!.date = line.substr("Date: ".length).trim()
        else if (line.startsWith("Merge:")) { }
        else if (!line.trim().length) { }
        else {
            c!.message += line.trim() + "\n"
        }
    }
    if (c) a.push(c)
    a.forEach(c => c.message = c.message.trim())
    return a
}

export async function log_opts(repo: string, opts: LogOpts) {
    const args = git_log_args(opts)
    const out = await run_repo_git_command(repo, args)
    const log = parse_log_output(out)
    return log
}
