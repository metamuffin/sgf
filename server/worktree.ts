import { basename, dirname } from "path/posix";
import { run_repo_git_command } from "./io";

export type FileListing = { [key: string]: "regular" | "directory" }

export async function worktree_ls(repo: string, commit: string, path: string): Promise<FileListing> {
    if (path == "") path = "."
    const result = await run_repo_git_command(repo, ["ls-files", "--with-tree", commit])
    const files = result.split("\n")

    const r: FileListing = {}
    for (const f of files) {
        const dir = dirname(f)
        const file = basename(f)
        // console.log(path, "-", dir, "-", file);
        if (dir == path) r[file] = "regular"
        else if (dir.startsWith(path + "/") || path == ".") {
            const a = dir.substr(path == "." ? 0 : path.length + 1)
            const b = a.split("/")[0]
            r[b] = "directory"
        }
    }
    return r
}
