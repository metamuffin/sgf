import { spawn } from "child_process";
import { join } from "path/posix";


export async function run_command(cwd: string, binary: string, args: string[]): Promise<string> {
    const proc = spawn(binary, args, { cwd })
    let stdout = "", stderr = ""
    proc.stdout.on("data", d => stdout += d.toString())
    proc.stderr.on("data", d => stderr += d.toString())
    await new Promise<void>(r => proc.on("close", () => r()))
    if (stderr) console.log(stderr);
    return stdout
}

export async function run_repo_command(repo: string, binary: string, args: string[]): Promise<string> {
    return await run_command(join(__dirname, "../repositories", repo + ".git"), binary, args)
}

export async function run_repo_git_command(repo: string, args: string[]) {
    return await run_command(join(__dirname, "../repositories", repo + ".git"), "git", args)
}