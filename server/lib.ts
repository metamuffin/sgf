import { readdir } from "fs/promises";
import { join } from "path/posix";

export const repo_directory = join(__dirname, "../repositories")


export async function list_repos(): Promise<string[]> {
    const dir_names = await readdir(repo_directory)
    return dir_names.map(n => n.substring(0, n.length - ".git".length))
}


export interface RepoMetadata {
    name: string,
    licence?: string
    primary_language?: string
}

// TODO
export async function get_repo_metadata(repo: string): Promise<RepoMetadata> {
    return {
        name: repo,
        licence: "unknown",
        primary_language: "typescript"
    }
}



